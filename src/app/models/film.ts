export class Film {
  noFilm: number;
  noRea: number;
  titre: string;
  duree: number;
  dateSortie: Date;
  budget: number;
  montantRecette: number;
  realisateurByNoRea: Realisateur;
  categorieByCodeCat: Categorie;


  constructor() {
    this.dateSortie = new Date();
    this.realisateurByNoRea = new Realisateur();
    this.categorieByCodeCat = new Categorie();
  }
}

export class Realisateur {
  noRea: number;
  nomRea: string;
  prenRea: string;

  constructor() {}
}

export class Categorie {
  codeCat: string;
  libelleCat: string;
  image: string;

  constructor() {}
}
