import {Film} from './film';

export class Acteur {
  noAct: number;
  nomAct: string;
  prenAct: string;
  dateNaiss: Date;
  dateDeces: Date;

  constructor() {}
}

export class Personnage {
  noFilm: number;
  noAct: number;
  nomPers: string;
  filmByNoFilm: Film;
  acteurByNoAct: Acteur;

  constructor() {
    this.filmByNoFilm = new Film();
    this.acteurByNoAct = new Acteur();
  }
}
