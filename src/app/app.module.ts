import { BrowserModule } from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import {RouterModule} from '@angular/router';
import {AppRoutingModule} from './app-routing.module';
import { FilmHomeComponent } from './components/film/film-home/film-home.component';
import { FilmDetailComponent } from './components/film/film-detail/film-detail.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import { FilmUpdateModalComponent } from './components/modal/film-update-modal/film-update-modal.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {registerLocaleData} from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import {HttpClientModule} from '@angular/common/http';
import { ConfirmDeleteModalComponent } from './components/modal/confirm-delete-modal/confirm-delete-modal.component';
import { ActeurHomeComponent } from './components/acteur/acteur-home/acteur-home.component';
import { ActeurDetailComponent } from './components/acteur/acteur-detail/acteur-detail.component';
import { FilmDetailModalComponent } from './components/modal/film-detail-modal/film-detail-modal.component';
import { PersonnageHomeComponent } from './components/personnage/personnage-home/personnage-home.component';
import { PersonnageDetailComponent } from './components/personnage/personnage-detail/personnage-detail.component';
import { PersonnageUpdateModalComponent } from './components/modal/personnage-update-modal/personnage-update-modal.component';
import { ActeurDetailModalComponent } from './components/modal/acteur-detail-modal/acteur-detail-modal.component';

registerLocaleData(localeFr);

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FilmHomeComponent,
    FilmDetailComponent,
    FilmUpdateModalComponent,
    ConfirmDeleteModalComponent,
    ActeurHomeComponent,
    ActeurDetailComponent,
    FilmDetailModalComponent,
    PersonnageHomeComponent,
    PersonnageDetailComponent,
    PersonnageUpdateModalComponent,
    ActeurDetailModalComponent
  ],
    imports: [
        BrowserModule,
        RouterModule,
        AppRoutingModule,
        FontAwesomeModule,
        NgbModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule
    ],
  providers: [
    {provide: LOCALE_ID, useValue: 'fr-FR'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
