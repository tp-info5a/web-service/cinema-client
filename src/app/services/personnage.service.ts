import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Personnage} from '../models/acteur';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PersonnageService {

  constructor(private http: HttpClient) { }

  public getAllPersonnage(): Observable<Array<Personnage>> {
    return this.http.get<Array<Personnage>>(environment.url_api + '/personnage', environment.httpOptions);
  }


  addPersonnage(personnage: Personnage): Observable<Personnage> {
    return this.http.put<Personnage>(environment.url_api + '/personnage', personnage, environment.httpOptions);
  }

  updatePersonnage(personnage: Personnage): Observable<Personnage> {
    return this.http.post<Personnage>(environment.url_api + '/personnage', personnage, environment.httpOptions);
  }

  deletePersonnage(personnage: Personnage): Observable<any> {
    return this.http.request<any>('delete',
      environment.url_api + '/personnage',
      {
        body: {noFilm: personnage.noFilm, noAct: personnage.noAct}
      }
    );
  }
}
