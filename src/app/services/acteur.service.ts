import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {Acteur} from '../models/acteur';

@Injectable({
  providedIn: 'root'
})
export class ActeurService {

  constructor(private http: HttpClient) { }

  public getAllActeur(): Observable<Array<Acteur>> {
    return this.http.get<Array<Acteur>>(environment.url_api + '/acteur', environment.httpOptions);
  }
}
