import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Categorie, Film, Realisateur} from '../models/film';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FilmService {

  constructor(private http: HttpClient) { }

  public getAllFilm(): Observable<Array<Film>> {
    return this.http.get<Array<Film>>(environment.url_api + '/film', environment.httpOptions);
  }

  public updateFilm(film: Film): Observable<Film> {
    return this.http.post<Film>(environment.url_api + '/film', film, environment.httpOptions);
  }

  public addFilm(film: Film): Observable<Film> {
    return this.http.put<Film>(environment.url_api + '/film', film, environment.httpOptions);
  }

  public getAllRealisateur(): Observable<Array<Realisateur>> {
    return this.http.get<Array<Realisateur>>(environment.url_api + '/realisateur', environment.httpOptions);
  }

  public getAllcategorie(): Observable<Array<Categorie>> {
    return this.http.get<Array<Categorie>>(environment.url_api + '/categorie', environment.httpOptions);
  }

  deleteFilm(film: Film): Observable<any> {
    return this.http.delete<any>(environment.url_api + '/film/' + film.noFilm, environment.httpOptions);
  }
}
