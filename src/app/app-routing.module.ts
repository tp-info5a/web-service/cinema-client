import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {FilmHomeComponent} from './components/film/film-home/film-home.component';
import {ActeurHomeComponent} from './components/acteur/acteur-home/acteur-home.component';
import {PersonnageHomeComponent} from './components/personnage/personnage-home/personnage-home.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'film', component: FilmHomeComponent },
  { path: 'acteur', component: ActeurHomeComponent },
  { path: 'personnage', component: PersonnageHomeComponent }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, {useHash: true})
  ]
})
export class AppRoutingModule { }
