import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonnageHomeComponent } from './personnage-home.component';

describe('PersonnageHomeComponent', () => {
  let component: PersonnageHomeComponent;
  let fixture: ComponentFixture<PersonnageHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonnageHomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonnageHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
