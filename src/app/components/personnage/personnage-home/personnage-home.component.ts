import { Component, OnInit } from '@angular/core';
import {Acteur, Personnage} from '../../../models/acteur';
import {PersonnageService} from '../../../services/personnage.service';
import {Film} from '../../../models/film';
import {ActeurService} from '../../../services/acteur.service';
import {FilmService} from '../../../services/film.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {PersonnageUpdateModalComponent} from '../../modal/personnage-update-modal/personnage-update-modal.component';

@Component({
  selector: 'app-personnage-home',
  templateUrl: './personnage-home.component.html',
  styleUrls: ['./personnage-home.component.css']
})
export class PersonnageHomeComponent implements OnInit {

  personnageList: Array<Personnage> = [];
  selectedPersonnageList: Array<Personnage> = [];
  acteurList: Array<Acteur> = [];
  filmList: Array<Film> = [];

  rechercherNomPersonnage = '';
  rechercherActeur: number = null;
  rechercherFilm: number = null;

  constructor(private personnageService: PersonnageService, private acteurService: ActeurService, private filmService: FilmService,
              private modalService: NgbModal) { }

  ngOnInit(): void {
    this.personnageService.getAllPersonnage().subscribe(
      (data) => {
        this.personnageList = data;
        this.selectedPersonnageList = data;
      }
    );
    this.acteurService.getAllActeur().subscribe(
      (data) => {
        this.acteurList = data;
      }
    );
    this.filmService.getAllFilm().subscribe(
      (data) => {
        this.filmList = data;
      }
    );
  }

  newPersonnage(): void {
    const modal = this.modalService.open(PersonnageUpdateModalComponent, {size: 'xl'});
    modal.componentInstance.acteurList = this.acteurList;
    modal.componentInstance.filmList = this.filmList;
    modal.componentInstance.newPersonnage = true;
    modal.result.then((result) => {
      if (result) {
        this.updateListPersonnage();
      }
    });
  }

  updateListPersonnage(): void {
    this.personnageService.getAllPersonnage().subscribe(
      (data) => {
        this.personnageList = data;
        this.selectedPersonnageList = data;
        this.cherchePersonnage();
      },
      (error) => console.error(error)
    );
  }

  cherchePersonnage(): void {
    this.selectedPersonnageList = this.personnageList
      .filter(personnage => personnage.nomPers.toUpperCase().includes(this.rechercherNomPersonnage.toUpperCase()))
      .filter(personnage => personnage.filmByNoFilm.noFilm === this.rechercherFilm || this.rechercherFilm === null)
      .filter(personnage => personnage.acteurByNoAct.noAct === this.rechercherActeur || this.rechercherActeur === null);
  }
}
