import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {faPen, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import {Acteur, Personnage} from '../../../models/acteur';
import {PersonnageUpdateModalComponent} from '../../modal/personnage-update-modal/personnage-update-modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Film} from '../../../models/film';
import {ConfirmDeleteModalComponent} from '../../modal/confirm-delete-modal/confirm-delete-modal.component';
import {PersonnageService} from '../../../services/personnage.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: '[app-personnage-detail]',
  templateUrl: './personnage-detail.component.html',
  styleUrls: ['./personnage-detail.component.css']
})
export class PersonnageDetailComponent implements OnInit {

  @Input() personnage: Personnage = new Personnage();
  @Input() acteurList: Array<Acteur> = [];
  @Input() filmList: Array<Film> = [];

  @Output() eventPersonnage = new EventEmitter<boolean>();


  faTrashAlt = faTrashAlt;
  faPen = faPen;

  constructor(private modalService: NgbModal, private personnageService: PersonnageService) { }

  ngOnInit(): void {
  }

  updatePersonnage(): void {
    const modal = this.modalService.open(PersonnageUpdateModalComponent, {size: 'xl'});
    modal.componentInstance.personnage = this.personnage;
    modal.componentInstance.acteurList = this.acteurList;
    modal.componentInstance.filmList = this.filmList;
    modal.componentInstance.newPersonnage = true;
    modal.result.then((result) => {
      if (result) {
        this.eventPersonnage.emit(result);
      }
    });
  }

  deletePersonnage(): void {
    const ngbModalRef = this.modalService.open(ConfirmDeleteModalComponent);
    ngbModalRef.result.then((result) => {
      if (result) {
        this.personnageService.deletePersonnage(this.personnage).subscribe(
          () => this.eventPersonnage.emit(true),
          error => console.error(error)
        );
      }
    });
  }
}
