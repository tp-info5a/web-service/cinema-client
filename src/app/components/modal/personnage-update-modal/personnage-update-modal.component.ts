import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Acteur, Personnage} from '../../../models/acteur';
import {Film} from '../../../models/film';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {PersonnageService} from '../../../services/personnage.service';

@Component({
  selector: 'app-personnage-update-modal',
  templateUrl: './personnage-update-modal.component.html',
  styleUrls: ['./personnage-update-modal.component.scss']
})
export class PersonnageUpdateModalComponent implements OnInit {

  @Input() personnage: Personnage = new Personnage();
  @Input() acteurList: Array<Acteur> = [];
  @Input() filmList: Array<Film> = [];
  @Input() newPersonnage;

  personnageForm: FormGroup;

  constructor(private modal: NgbActiveModal, private formBuilder: FormBuilder, private personnageService: PersonnageService) { }

  ngOnInit(): void {
    this.personnageForm = this.formBuilder.group({
      nom: [this.personnage.nomPers, Validators.required],
      acteur: [this.personnage.noAct, Validators.required],
      film: [this.personnage.noFilm, Validators.required],
    });
  }

  close(): void {
    this.modal.close(false);
  }

  validate(): void {
    this.personnage.nomPers = this.personnageForm.value.nom;
    this.personnage.noAct = this.personnageForm.value.acteur;
    this.personnage.noFilm = this.personnageForm.value.film;
    this.personnage.acteurByNoAct = this.acteurList.find((a) => a.noAct === this.personnage.noAct);
    this.personnage.filmByNoFilm = this.filmList.find((f) => f.noFilm === this.personnage.noFilm);

    let personnageObservable: Observable<Personnage>;
    if (this.newPersonnage) {
      personnageObservable = this.personnageService.addPersonnage(this.personnage);
    } else {
      personnageObservable = this.personnageService.updatePersonnage(this.personnage);
    }
    personnageObservable.subscribe(
      () => this.modal.close(true),
      error => console.error(error)
    );
  }
}
