import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonnageUpdateModalComponent } from './personnage-update-modal.component';

describe('PersonnageUpdateModalComponent', () => {
  let component: PersonnageUpdateModalComponent;
  let fixture: ComponentFixture<PersonnageUpdateModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonnageUpdateModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonnageUpdateModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
