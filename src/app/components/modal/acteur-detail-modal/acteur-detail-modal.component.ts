import {Component, Input, OnInit} from '@angular/core';
import {Acteur, Personnage} from '../../../models/acteur';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {PersonnageService} from '../../../services/personnage.service';

@Component({
  selector: 'app-acteur-detail-modal',
  templateUrl: './acteur-detail-modal.component.html',
  styleUrls: ['./acteur-detail-modal.component.scss']
})
export class ActeurDetailModalComponent implements OnInit {

  @Input() acteur: Acteur = new Acteur();

  personnageList: Array<Personnage> = [];

  constructor(private modal: NgbActiveModal, private personnageService: PersonnageService) { }

  ngOnInit(): void {
    console.log(this.acteur);
    this.personnageService.getAllPersonnage().subscribe(
      (data) => {
        this.personnageList = data.filter(personnage => personnage.acteurByNoAct.noAct === this.acteur.noAct);
        console.log(this.personnageList, data);
      },
      error => console.error(error)
    );
  }

  close(): void {
    this.modal.close(true);
  }

}
