import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActeurDetailModalComponent } from './acteur-detail-modal.component';

describe('ActeurDetailModalComponent', () => {
  let component: ActeurDetailModalComponent;
  let fixture: ComponentFixture<ActeurDetailModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActeurDetailModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActeurDetailModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
