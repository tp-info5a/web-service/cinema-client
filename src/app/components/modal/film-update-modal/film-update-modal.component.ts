import {Component, Input, OnInit} from '@angular/core';
import {Categorie, Film, Realisateur} from '../../../models/film';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FilmService} from '../../../services/film.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-film-update-modal',
  templateUrl: './film-update-modal.component.html',
  styleUrls: ['./film-update-modal.component.scss']
})
export class FilmUpdateModalComponent implements OnInit {

  @Input() film: Film = new Film();
  @Input() realisateurList: Array<Realisateur>;
  @Input() categorieList: Array<Categorie>;

  filmForm: FormGroup;

  constructor(private modal: NgbActiveModal, private formBuilder: FormBuilder, private filmService: FilmService) { }

  ngOnInit(): void {
    this.filmForm = this.formBuilder.group({
      titre: [this.film.titre, Validators.required],
      duree: [this.film.duree, Validators.required],
      date: [this.film.dateSortie, Validators.required],
      budget: [this.film.budget, Validators.required],
      montant: [this.film.montantRecette, Validators.required],
      realisateur: ['', Validators.required],
      categorie: ['', Validators.required],
    });
  }

  close(): void {
    this.modal.close(false);
  }

  validate(): void {
    this.film.titre = this.filmForm.value.titre;
    this.film.duree = this.filmForm.value.duree;
    this.film.dateSortie = new Date(this.filmForm.value.date);
    this.film.budget = this.filmForm.value.budget;
    this.film.montantRecette = this.filmForm.value.montant;
    this.film.realisateurByNoRea = this.realisateurList.find((r) => r.noRea === this.filmForm.value.realisateur);
    this.film.noRea = this.film.realisateurByNoRea.noRea;
    this.film.categorieByCodeCat = this.categorieList.find((c) => c.codeCat === this.filmForm.value.categorie);

    let filmObservable: Observable<Film>;
    if (this.film.noFilm === undefined) {
      filmObservable = this.filmService.addFilm(this.film);
    } else {
      filmObservable = this.filmService.updateFilm(this.film);
    }
    filmObservable.subscribe(
      () => this.modal.close(true),
      error => console.error(error)
      );
    // appel service + close(true)
  }
}
