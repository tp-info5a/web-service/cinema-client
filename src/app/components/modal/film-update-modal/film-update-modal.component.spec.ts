import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilmUpdateModalComponent } from './film-update-modal.component';

describe('FilmUpdateModalComponent', () => {
  let component: FilmUpdateModalComponent;
  let fixture: ComponentFixture<FilmUpdateModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilmUpdateModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FilmUpdateModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
