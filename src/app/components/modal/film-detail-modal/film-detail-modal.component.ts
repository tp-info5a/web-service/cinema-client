import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Film} from '../../../models/film';
import {Personnage} from '../../../models/acteur';
import {PersonnageService} from '../../../services/personnage.service';

@Component({
  selector: 'app-film-detail-modal',
  templateUrl: './film-detail-modal.component.html',
  styleUrls: ['./film-detail-modal.component.scss']
})
export class FilmDetailModalComponent implements OnInit {

  @Input() film: Film = new Film();

  personnageList: Array<Personnage> = [];

  constructor(private modal: NgbActiveModal, private personnageService: PersonnageService) { }

  ngOnInit(): void {
    console.log(this.film);
    this.personnageService.getAllPersonnage().subscribe(
      (data) => {
        this.personnageList = data.filter(personnage => personnage.filmByNoFilm.noFilm === this.film.noFilm);
        console.log(this.personnageList, data);
      },
      error => console.error(error)
    );
  }

  close(): void {
    this.modal.close(true);
  }
}
