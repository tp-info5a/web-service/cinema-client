import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActeurHomeComponent } from './acteur-home.component';

describe('ActeurHomeComponent', () => {
  let component: ActeurHomeComponent;
  let fixture: ComponentFixture<ActeurHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActeurHomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActeurHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
