import { Component, OnInit } from '@angular/core';
import {Acteur} from '../../../models/acteur';
import {ActeurService} from '../../../services/acteur.service';

@Component({
  selector: 'app-acteur-home',
  templateUrl: './acteur-home.component.html',
  styleUrls: ['./acteur-home.component.css']
})
export class ActeurHomeComponent implements OnInit {

  acteurList: Array<Acteur> = [];
  acteurSelected: Array<Acteur> = [];

  rechercheNom = '';
  rechercheDateNaiss = '';
  rechercheDateDeces = '';

  constructor(private acteurService: ActeurService) { }

  ngOnInit(): void {
    this.acteurService.getAllActeur().subscribe(
      (data) => {
        this.acteurList = data;
        this.acteurSelected = data;
      },
      (error) => console.error(error)
    );
  }

  chercheActeur(): void {
    console.log(this.acteurList, this.rechercheDateNaiss);
    this.acteurSelected = this.acteurList.filter(acteur => (acteur.nomAct + ' ' + acteur.prenAct).toUpperCase().includes(this.rechercheNom.toUpperCase()))
      .filter(acteur => (new Date(acteur.dateNaiss)).getTime() >= (new Date(this.rechercheDateNaiss)).getTime() || this.rechercheDateNaiss === '')
      .filter(acteur => (acteur.dateDeces !== null && (new Date(acteur.dateDeces)).getTime() <= (new Date(this.rechercheDateDeces)).getTime()) || this.rechercheDateDeces === '');
  }
}
