import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActeurDetailComponent } from './acteur-detail.component';

describe('ActeurDetailComponent', () => {
  let component: ActeurDetailComponent;
  let fixture: ComponentFixture<ActeurDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActeurDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActeurDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
