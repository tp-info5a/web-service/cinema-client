import {Component, Input, OnInit} from '@angular/core';
import {Acteur} from '../../../models/acteur';
import {faSearch} from '@fortawesome/free-solid-svg-icons';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActeurDetailModalComponent} from '../../modal/acteur-detail-modal/acteur-detail-modal.component';

@Component({
  // tslint:disable-next-line:component-selector
  selector: '[app-acteur-detail]',
  templateUrl: './acteur-detail.component.html',
  styleUrls: ['./acteur-detail.component.css']
})
export class ActeurDetailComponent implements OnInit {

  @Input() acteur: Acteur = new Acteur();

  faSearch = faSearch;

  constructor(private modalService: NgbModal) { }

  ngOnInit(): void {
  }

  showActeur(): void {
    const ngbModalRef = this.modalService.open(ActeurDetailModalComponent);
    ngbModalRef.componentInstance.acteur = this.acteur;
  }
}
