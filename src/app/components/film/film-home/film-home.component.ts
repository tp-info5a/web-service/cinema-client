import { Component, OnInit } from '@angular/core';
import {Categorie, Film, Realisateur} from '../../../models/film';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FilmUpdateModalComponent} from '../../modal/film-update-modal/film-update-modal.component';
import {FilmService} from '../../../services/film.service';

@Component({
  selector: 'app-film-home',
  templateUrl: './film-home.component.html',
  styleUrls: ['./film-home.component.css']
})
export class FilmHomeComponent implements OnInit {

  filmList: Array<Film> = [];
  filmSelected: Array<Film> = [];
  realisateurList: Array<Realisateur> = [];
  categorieList: Array<Categorie> = [];

  rechercheTitre = '';
  rechercheRealisateur: number = null;
  rechercheCategorie = '';

  constructor(private modalService: NgbModal, private filmService: FilmService) { }

  ngOnInit(): void {
    this.filmService.getAllFilm().subscribe(
      (data) => {
        this.filmList = data;
        this.filmSelected = data;
      },
      (error) => console.error(error)
    );
    this.filmService.getAllRealisateur().subscribe(
      (data) => this.realisateurList = data,
      (error) => console.error(error)
    );

    this.filmService.getAllcategorie().subscribe(
      (data) => this.categorieList = data,
      (error) => console.error(error)
    );
  }

  newFilm(): void {
    const modal = this.modalService.open(FilmUpdateModalComponent, {size: 'xl'});
    modal.componentInstance.film = new Film();
    modal.componentInstance.realisateurList = this.realisateurList;
    modal.componentInstance.categorieList = this.categorieList;
    modal.result.then((result) => {
      if (result) {
        this.updateListFilm();
      }
    });
  }

  updateListFilm(): void {
    this.filmService.getAllFilm().subscribe(
      (data) => {
        this.filmList = data;
        this.filmSelected = data;
        this.chercheFilm();
      },
      (error) => console.error(error)
    );
  }

  chercheFilm(): void {
    this.filmSelected = this.filmList
      .filter(film => film.titre.toUpperCase().includes(this.rechercheTitre.toUpperCase()))
      .filter(film => (film.realisateurByNoRea.noRea === this.rechercheRealisateur) || (this.rechercheRealisateur === null))
      .filter(film => film.categorieByCodeCat.codeCat.toUpperCase().includes(this.rechercheCategorie.toUpperCase()));
  }
}
