import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Categorie, Film, Realisateur} from '../../../models/film';
import {faPen, faSearch, faTrashAlt} from '@fortawesome/free-solid-svg-icons';
import {FilmUpdateModalComponent} from '../../modal/film-update-modal/film-update-modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FilmService} from '../../../services/film.service';
import {ConfirmDeleteModalComponent} from '../../modal/confirm-delete-modal/confirm-delete-modal.component';
import {FilmDetailModalComponent} from '../../modal/film-detail-modal/film-detail-modal.component';

@Component({
  // tslint:disable-next-line:component-selector
  selector: '[app-film-detail]',
  templateUrl: './film-detail.component.html',
  styleUrls: ['./film-detail.component.css']
})
export class FilmDetailComponent implements OnInit {

  @Input() film: Film = new Film();
  @Input() realisateurList: Array<Realisateur>;
  @Input() categorieList: Array<Categorie>;

  @Output() eventFilm = new EventEmitter<boolean>();

  faSearch = faSearch;
  faTrashAlt = faTrashAlt;
  faPen = faPen;

  constructor(private modalService: NgbModal, private filmService: FilmService) { }

  ngOnInit(): void {
  }

  updateFilm(): void {
    const ngbModalRef = this.modalService.open(FilmUpdateModalComponent, {size: 'xl'});
    ngbModalRef.componentInstance.film = this.film;
    ngbModalRef.componentInstance.realisateurList = this.realisateurList;
    ngbModalRef.componentInstance.categorieList = this.categorieList;
    ngbModalRef.result.then((result) => {
      if (result) {
        this.eventFilm.emit(result);
      }
    });
  }

  deleteFilm(): void {
    const ngbModalRef = this.modalService.open(ConfirmDeleteModalComponent);
    ngbModalRef.result.then((result) => {
      if (result) {
        this.filmService.deleteFilm(this.film).subscribe(
          () => this.eventFilm.emit(true),
          error => console.error(error)
        );
      }
    });
  }

  showFilm(): void {
    const ngbModalRef = this.modalService.open(FilmDetailModalComponent, {size: 'xl'});
    ngbModalRef.componentInstance.film = this.film;
  }
}
